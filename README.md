# Write simple units of code

Sample project to exemplify the usage of JavaScript `forEach` on Protractor tests

## Installation

Run `npm i` to install the project dev dependencies.

## Running the tests

Run `npm t` to run the e2e tests.

### Build status

[![Build Status](https://gitlab.com/wlsf82/write-simple-units-of-code/badges/master/build.svg)](https://gitlab.com/wlsf82/write-simple-units-of-code)